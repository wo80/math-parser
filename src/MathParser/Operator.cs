﻿using System;

namespace MathParser;

/// <summary>
/// Class for handling arithmetic operations.
/// </summary>
internal class Operator
{
    /// <summary>
    /// Returns true, whether the operation can be handled by the parser.
    /// </summary>
    public static bool IsKnownOperator(char op)
    {
        switch (op)
        {
            case '+':
            case '-':
            case '/':
            case '*':
            case '^': return true;
        }

        return false;
    }

    /// <summary>
    /// Returns whether the operator is left-associative.
    /// </summary>
    public static bool IsLeftAssociative(string op) => !op.Equals("^");

    /// <summary>
    /// Returns whether the operator is right-associative.
    /// </summary>
    public static bool IsRightAssociative(string op) => op.Equals("^");

    /// <summary>
    /// Returns the priority of the given operator.
    /// </summary>
    public static int GetPrecedence(string op)
    {
        switch (op)
        {
            case "(": return 1; // Include left paren to simplify parsing.
            case "+": return 4;
            case "-": return 4;
            case "/": return 6;
            case "*": return 6;
            case "^": return 8;
            case "~": return 9; // Unary minus.
        }

        return 0;
    }

    /// <summary>
    /// Evaluate a unary operator.
    /// </summary>
    public static double Evaluate(string o, double op)
    {
        switch (o[0])
        {
            case '~': return -op;
        }

        return double.NaN;
    }

    /// <summary>
    /// Evaluates a binary operator.
    /// </summary>
    public static double Evaluate(string o, double op1, double op2)
    {
        switch (o[0])
        {
            case '+': return op1 + op2;
            case '-': return op1 - op2;
            case '/': return op1 / op2;
            case '*': return op1 * op2;
            case '^': return Math.Pow(op1, op2);
        }

        return double.NaN;
    }

    /// <summary>
    /// Returns whether the operator is a binary operator.
    /// </summary>
    public static bool IsBinaryOperator(string op)
    {
        switch (op[0])
        {
            case '+':
            case '-':
            case '/':
            case '*':
            case '^': return true;
            case '~': return false; // Unary minus
        }

        return true;
    }

    /// <summary>
    /// Returns true, whether the operation can be handled by the parser.
    /// </summary>
    public static bool IsUnaryPrefixOperator(char op) => op == '-' || op == '~';

    /// <summary> 
    /// Gets a value indicating whether the token is a unary minus.
    /// </summary>
    public static bool IsUnaryMinus(string op) => op.Equals("~");
}
