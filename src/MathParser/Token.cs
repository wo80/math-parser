﻿namespace MathParser;

public enum TokenType { Operand, Operator, Parenthesis, Symbol, Function, Separator }

public class Token
{
    public Token(TokenType type, string text, int position)
    {
        Type = type;
        Text = text;
        Position = position;
    }

    public Token(TokenType type, string text, int position, double value)
        : this(type, text, position)
    {
        Value = value;
    }

    #region Public properties

    /// <summary>
    /// Gets the position of token in the input expression.
    /// </summary>
    public int Position { get; }

    /// <summary>
    /// Gets the token type.
    /// </summary>
    public TokenType Type { get; }

    /// <summary>
    /// Gets the text that is represented by the token.
    /// </summary>
    public string Text { get; }

    /// <summary>
    /// If token is of type operand, the value of the operand is returned. Otherwise, zero.
    /// </summary>
    public double Value { get; }

    #endregion

    public override string ToString() => Text;
}
