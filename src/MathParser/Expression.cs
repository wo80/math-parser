﻿namespace MathParser;

using System;
using System.Collections.Generic;
using System.Linq;

public class Expression
{
    private Token[] expression;

    /// <summary>
    /// Gets the variables dictionary.
    /// </summary>
    public Dictionary<string, double> Variables { get; }

    /// <summary>
    /// Gets or sets a value indicating to how many decimal places the expression
    /// evaluation result should be rounded (default = 0, no rounding).
    /// </summary>
    public static int Round { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="Parser"/> class.
    /// </summary>
    /// <param name="expression">The input expression.</param>
    public Expression(Token[] expression, Dictionary<string, double> variables)
    {
        this.expression = expression;

        Variables = variables;
    }

    /// <summary>
    /// Simplify the expression.
    /// </summary>
    /// <remarks>
    /// Be careful using the <c>Simplify</c> method when using user-defined functions. For example, when
    /// registering a function "time()" which is supposed to return the current Unix time-stamp, evaluation
    /// of the expression will actually always return the time-stamp when <c>Simplify</c> was called.
    /// </remarks>
    public Expression Simplify()
    {
        var functions = Parser.Functions;

        var stack = new Stack<Token>();

        foreach (var token in expression)
        {
            if (token.Type == TokenType.Operand)
            {
                stack.Push(token);
            }
            else if (token.Type == TokenType.Operator)
            {
                if (Operator.IsBinaryOperator(token.Text))
                {
                    var op2 = stack.Pop();
                    var op1 = stack.Pop();

                    if (op1.Type == TokenType.Operand && op2.Type == TokenType.Operand)
                    {
                        var text = string.Format("[{0} {1} {2}]", op1.Text, op2.Text, token.Text);
                        var value = Operator.Evaluate(token.Text, op1.Value, op2.Value);

                        stack.Push(new Token(TokenType.Operand, text, op1.Position, value));
                    }
                    else
                    {
                        stack.Push(op1);
                        stack.Push(op2);
                        stack.Push(token);
                    }
                }
                else
                {
                    var op = stack.Pop();

                    if (op.Type == TokenType.Operand)
                    {
                        var text = string.Format("[{0} {1}]", op.Text, token.Text);
                        var value = Operator.Evaluate(token.Text, op.Value);

                        stack.Push(new Token(TokenType.Operand, text, token.Position, value));
                    }
                    else
                    {
                        stack.Push(op);
                        stack.Push(token);
                    }
                }
            }
            else if (token.Type == TokenType.Function)
            {
                int order = functions.GetOrder(token.Text);

                if (order == 0)
                {
                    var text = string.Format("[{0}]", token.Text);

                    functions.TryGet(token.Text, out Func0 func);

                    stack.Push(new Token(TokenType.Operand, text, token.Position, func()));
                }
                else if (order == 1)
                {
                    var op = stack.Pop();

                    if (op.Type == TokenType.Operand)
                    {
                        var text = string.Format("[{0} {1}]", op.Text, token.Text);

                        functions.TryGet(token.Text, out Func1 func);

                        stack.Push(new Token(TokenType.Operand, text, token.Position, func(op.Value)));
                    }
                    else
                    {
                        stack.Push(op);
                        stack.Push(token);
                    }
                }
                else if (order == 2)
                {
                    var op1 = stack.Pop();
                    var op2 = stack.Pop();

                    if (op1.Type == TokenType.Operand && op2.Type == TokenType.Operand)
                    {
                        var text = string.Format("[{0} {1} {2}]", op1.Text, op2.Text, token.Text);

                        functions.TryGet(token.Text, out Func2 func);

                        stack.Push(new Token(TokenType.Operand, text, token.Position, func(op1.Value, op2.Value)));
                    }
                    else
                    {
                        stack.Push(op1);
                        stack.Push(op2);
                        stack.Push(token);
                    }
                }
            }
            else
            {
                stack.Push(token);
            }
        }

        expression = stack.Reverse().ToArray();

        return this;
    }

    /// <summary>
    /// Evaluate the expression.
    /// </summary>
    public double Evaluate()
    {
        int length = expression.Length;

        if (length == 0)
        {
            throw new InvalidOperationException();
        }

        var functions = Parser.Functions;

        var stack = new double[length];
        int index = 0;

        try
        {
            for (int i = 0; i < length; i++)
            {
                var token = expression[i];

                if (token.Type == TokenType.Operand)
                {
                    stack[index++] = token.Value;
                }
                else if (token.Type == TokenType.Operator)
                {
                    if (Operator.IsBinaryOperator(token.Text))
                    {
                        double op2 = stack[--index];
                        double op1 = stack[--index];

                        stack[index++] = Operator.Evaluate(token.Text, op1, op2);
                    }
                    else
                    {
                        double op = stack[--index];
                        stack[index++] = Operator.Evaluate(token.Text, op);
                    }
                }
                else if (token.Type == TokenType.Function)
                {
                    if (functions.TryGet(token.Text, out Func0 func0))
                    {
                        stack[index++] = func0();
                    }
                    if (functions.TryGet(token.Text, out Func1 func1))
                    {
                        double op = stack[--index];

                        stack[index++] = func1(op);
                    }
                    else if (functions.TryGet(token.Text, out Func2 func2))
                    {
                        double op2 = stack[--index];
                        double op1 = stack[--index];

                        stack[index++] = func2(op1, op2);
                    }
                }
                else if (token.Type == TokenType.Symbol)
                {
                    stack[index++] = Variables[token.Text];
                }
                else
                {
                    throw new ParserException("Unknown token", token);
                }
            }
        }
        catch (Exception e)
        {
            throw new ParserException("Invalid postfix expression", e);
        }

        if (index != 1)
        {
            throw new ParserException("Error evaluating postfix expression");
        }

        if (Round > 0)
        {
            return Math.Round(stack[0], Math.Min(15, Round));
        }

        return stack[0];
    }

    /// <summary>
    /// Compile the expression to IL.
    /// </summary>
    /// <typeparam name="T">The returned delegate type, for example <c>Func&lt;double, double&gt;</c>.</typeparam>
    /// <returns>Returns the compiled expression as a delegate.</returns>
    /// <example>
    /// <code>
    /// var e = Parser.Parse("x + y", new[] { "x", "y" });
    /// var f = e.Compile&lt;Func&lt;double, double, double&gt;&gt;();
    /// var result = f(1.0, 1.0);
    /// </code>
    /// </example>
    public T Compile<T>() where T : Delegate
    {
        return ExpressionCompiler.Create<T>(expression, Variables.Keys);
    }

    /// <summary>
    /// Compile an expression with one argument.
    /// </summary>
    public Func1 Compile1()
    {
        return ExpressionCompiler.Create<Func1>(expression, Variables.Keys);
    }

    /// <summary>
    /// Compile an expression with two arguments.
    /// </summary>
    public Func2 Compile2()
    {
        return ExpressionCompiler.Create<Func2>(expression, Variables.Keys);
    }

    /// <summary>
    /// Compile an expression with three arguments.
    /// </summary>
    public Func3 Compile3()
    {
        return ExpressionCompiler.Create<Func3>(expression, Variables.Keys);
    }

    /// <summary>
    /// Returns a string that represents the postfix expression.
    /// </summary>
    public override string ToString()
    {
        return string.Join(" ", expression.Select(t => t.Text));
    }
}
