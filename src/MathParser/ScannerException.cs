﻿using System;

namespace MathParser;

public class ScannerException : Exception
{
    public int Position { get; }

    public ScannerException(string message)
        : this(message, -1)
    {
    }

    public ScannerException(string message, int position)
        : base(message)
    {
        Position = position;
    }
}
