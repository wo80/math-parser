﻿using System;

namespace MathParser;

public class ParserException : Exception
{
    public ParserException(string message)
        : base(message)
    {
    }

    public ParserException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    public ParserException(string message, Token token)
        : base(message)
    {
        Token = token;
    }

    public Token Token { get; }
}
