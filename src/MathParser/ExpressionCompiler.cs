﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using LExpression = System.Linq.Expressions.Expression;

namespace MathParser;

class ExpressionCompiler
{
    public static T Create<T>(Token[] expression, IEnumerable<string> variables)
        where T : Delegate
    {
        return Create<T>(expression, GetParams(variables)).Compile();
    }

    private static Expression<T> Create<T>(Token[] expression, Dictionary<string, ParameterExpression> parameters)
        where T : Delegate
    {
        var functions = Parser.Functions;

        var lambda = new Stack<LExpression>();

        foreach (var token in expression)
        {
            if (token.Type == TokenType.Operand)
            {
                lambda.Push(LExpression.Constant(token.Value, typeof(double)));
            }
            else if (token.Type == TokenType.Operator)
            {
                if (Operator.IsBinaryOperator(token.Text))
                {
                    var op2 = lambda.Pop();
                    var op1 = lambda.Pop();

                    lambda.Push(GetBinaryOperator(token.Text, op1, op2));
                }
                else if (Operator.IsUnaryMinus(token.Text))
                {
                    var op = lambda.Pop();
                    lambda.Push(LExpression.Negate(op));
                }
                else
                {
                    throw new Exception("Unexpected operator token.");
                }
            }
            else if (token.Type == TokenType.Function)
            {
                if (functions.TryGet(token.Text, out Func0 func0))
                {
                    var op = lambda.Pop();
                    lambda.Push(LExpression.Invoke(LExpression.Constant(func0)));
                }
                else if (functions.TryGet(token.Text, out Func1 func1))
                {
                    var op = lambda.Pop();
                    lambda.Push(LExpression.Invoke(LExpression.Constant(func1), new LExpression[] { op }));
                }
                else if (functions.TryGet(token.Text, out Func2 func2))
                {
                    var op2 = lambda.Pop();
                    var op1 = lambda.Pop();
                    lambda.Push(LExpression.Invoke(LExpression.Constant(func2), new LExpression[] { op1, op2 }));
                }
            }
            else if (token.Type == TokenType.Symbol)
            {
                if (parameters.TryGetValue(token.Text, out var variable))
                {
                    lambda.Push(variable);
                }
                else
                {
                    throw new Exception("Unknown symbol token.");
                }
            }
        }

        return LExpression.Lambda<T>(lambda.Pop(), parameters.Values);
    }

    private static Dictionary<string, ParameterExpression> GetParams(IEnumerable<string> variables)
    {
        Dictionary<string, ParameterExpression> parameters = new();

        foreach (var variable in variables)
        {
            parameters.Add(variable, LExpression.Parameter(typeof(double), variable));
        }

        return parameters;
    }

    private static LExpression GetBinaryOperator(string op, LExpression t1, LExpression t2)
    {
        switch (op)
        {
            case "+": return LExpression.Add(t1, t2);
            case "-": return LExpression.Subtract(t1, t2);
            case "*": return LExpression.Multiply(t1, t2);
            case "/": return LExpression.Divide(t1, t2);
            case "^": return LExpression.Power(t1, t2);
        }

        throw new Exception("Unknown operator.");
    }
}
