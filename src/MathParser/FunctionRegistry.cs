﻿namespace MathParser;

using System;
using System.Collections.Generic;

public delegate double Func0();
public delegate double Func1(double x);
public delegate double Func2(double x, double y);
public delegate double Func3(double x, double y, double z);

public class FunctionRegistry
{
    private readonly Dictionary<string, Func0> func0 = new();

    private readonly Dictionary<string, Func1> func1 = new()
    {
#if NET8_0_OR_GREATER
        { "acosh", Math.Acosh },
        { "asinh", Math.Asinh },
        { "atanh", Math.Atanh },
#endif
        { "cos", Math.Cos },
        { "sin", Math.Sin },
        { "tan", Math.Tan },
        { "cosh", Math.Cosh },
        { "sinh", Math.Sinh },
        { "tanh", Math.Tanh },
        { "acos", Math.Acos },
        { "asin", Math.Asin },
        { "atan", Math.Atan },
        { "sqrt", Math.Sqrt },
        { "cbrt", x => Math.Pow(x, 1d / 3d) },
        { "ln", Math.Log },
        { "log", Math.Log },
        { "log2", x => Math.Log(x, 2d) },
        { "log10", x => Math.Log(x, 10) },
        { "exp", Math.Exp },
        { "floor", Math.Floor },
        { "ceil", Math.Ceiling },
        { "abs", Math.Abs }
    };

    private readonly Dictionary<string, Func2> func2 = new()
    {
#if NET8_0_OR_GREATER
        { "hypot", Double.Hypot },
#endif
        { "pow", Math.Pow },
        { "max", Math.Max },
        { "min", Math.Min },
        { "atan2", Math.Atan2 },
        { "mod", (x, y) => x % y }
        //{ "+", (x, y) => x + y },
        //{ "-", (x, y) => x - y },
        //{ "*", (x, y) => x * y },
        //{ "/", (x, y) => x / y },
        //{ "^", (x, y) => Math.Pow(x, y) }
    };

    public void Add(string name, Func0 function)
    {
        func0[name] = function;
    }

    public void Add(string name, Func1 function)
    {
        func1[name] = function;
    }

    public void Add(string name, Func2 function)
    {
        func2[name] = function;
    }

    public bool TryGet(string name, out Func0 function)
    {
        return func0.TryGetValue(name, out function);
    }

    public bool TryGet(string name, out Func1 function)
    {
        return func1.TryGetValue(name, out function);
    }

    public bool TryGet(string name, out Func2 function)
    {
        return func2.TryGetValue(name, out function);
    }

    public bool IsKnown(string name)
    {
        return func1.ContainsKey(name) || func2.ContainsKey(name) || func0.ContainsKey(name);
    }

    public int GetOrder(string name)
    {
        return func1.ContainsKey(name) ? 1 : (func2.ContainsKey(name) ? 2 : (func0.ContainsKey(name) ? 0 : -1));
    }
}
