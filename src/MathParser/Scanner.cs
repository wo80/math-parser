﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace MathParser;

/// <summary>
/// The static <see cref="Scanner"/> class is responsible for spitting a math formula
/// (given as a string) into an array of tokens.
/// </summary>
/// <example>
/// var result = Scanner.Tokenize("5-(2*3-4)");
/// </example>
public static class Scanner
{
    private static readonly char decimalSeperator;

    static Scanner()
    {
        decimalSeperator = Parser.NumberFormat.NumberDecimalSeparator[0];
    }

    /// <summary>
    /// Static function, which splits the input expression into a token array.
    /// </summary>
    /// <param name="expression">The input math formula.</param>
    /// <returns>Array of <see cref="Token"/>s.</returns>
    /// <exception cref="ScannerException">
    /// Occurs, if an invalid character is found in the input string, parentheses
    /// are unbalanced or an operand could not be parsed to double.
    /// </exception>
    public static Token[] Tokenize(string expression)
    {
        int position = 0;

        var token = new List<Token>();

        int length = expression.Length;

        // Counts opening and closing brackets
        int balance = 0;

        // Scan the input expression
        while (position < length)
        {
            char c = expression[position];

            if (char.IsWhiteSpace(c))
            {
                // Ignore whitespace
            }
            else if (Operator.IsKnownOperator(c))
            {
                // Detect unary prefix operators.
                if (Operator.IsUnaryPrefixOperator(c))
                {
                    int count = token.Count;

                    if (count == 0 || token[count - 1].Type == TokenType.Operator
                                   || token[count - 1].Type == TokenType.Separator
                                   || token[count - 1].Text.Equals("("))
                    {
                        // Unary minus is the only prefix operator.
                        c = '~';
                    }
                }

                token.Add(new Token(TokenType.Operator, c.ToString(), position));
            }
            else if (c == '(')
            {
                token.Add(new Token(TokenType.Parenthesis, "(", position));
                balance++;
            }
            else if (c == ')')
            {
                balance--;

                if (balance < 0)
                {
                    throw new ScannerException("Unbalanced parenthesis", position);
                }

                /*
                int count = token.Count;

                // Empty parenthesis are not allowed
                if (count == 0 || token[count - 1].Text.Equals("("))
                {
                    throw new ScannerException("Invalid parenthesis", position);
                }
                //*/

                token.Add(new Token(TokenType.Parenthesis, ")", position));
            }
            else if (char.IsDigit(c) || c == decimalSeperator)
            {
                token.Add(ReadNumber(expression, length, ref position));
            }
            else if (char.IsLetter(c) || c == '_')
            {
                token.Add(ReadSymbol(expression, length, ref position));
            }
            else if (c == Parser.ArgumentSeparator)
            {
                token.Add(new Token(TokenType.Separator, c.ToString(), position));
            }
            else
            {
                throw new ScannerException("Unsupported input character", position);
            }

            position++;
        }

        if (balance != 0)
        {
            throw new ScannerException("Unbalanced parenthesis", position - 1);
        }

        return token.ToArray();
    }

    /// <summary>
    /// Reads a decimal number from the input expression.
    /// </summary>
    /// <returns>Token of type <see cref="TokenType.Operand"/>.</returns>
    private static Token ReadNumber(string expression, int length, ref int position)
    {
        // TODO: check if using regex "d*\.?\d+([eE][-+]?\d+)?" is faster.

        int start = position;
        int i = position;

        var sb = new StringBuilder(16);

        // Read digits before the decimal point.
        consumeDigits();

        if (i < length && expression[i] == decimalSeperator)
        {
            sb.Append(decimalSeperator);
            i++;

            // Read digits after the decimal point.
            consumeDigits();
        }

        // Read exponential part.
        if (i < length && char.ToLower(expression[i]) == 'e')
        {
            if (expression[i - 1] == decimalSeperator)
            {
                throw new ScannerException("Invalid floating point format", i);
            }

            sb.Append('e');
            i++;

            if (i == length)
            {
                throw new ScannerException("Invalid floating point format", i);
            }

            if (expression[i] == '+' || expression[i] == '-')
            {
                sb.Append(expression[i]);
                i++;
            }

            if (i == length)
            {
                throw new ScannerException("Invalid floating point format", i);
            }

            if (!char.IsDigit(expression[i]))
            {
                throw new ScannerException("Invalid floating point format", i);
            }

            // Read exponential digits.
            consumeDigits();
        }

        string text = sb.ToString();

        if (!double.TryParse(text, NumberStyles.Float, Parser.NumberFormat, out var value))
        {
            throw new ScannerException("Invalid floating point format", start);
        }

        position = (i < length) ? i - 1 : i;

        return new Token(TokenType.Operand, text, start, value);

        void consumeDigits()
        {
            while (i < length && char.IsDigit(expression[i]))
            {
                sb.Append(expression[i]);
                i++;
            }
        }
    }

    /// <summary>
    /// Reads a symbol (function, variable or constant) from the input expression.
    /// </summary>
    /// <returns>Token of type <see cref="TokenType.Symbol"/>.</returns>
    private static Token ReadSymbol(string expression, int length, ref int position)
    {
        var type = TokenType.Symbol;

        int start = position;
        int i = position;

        var sb = new StringBuilder(8);

        while (i < length && (char.IsLetterOrDigit(expression[i]) || expression[i] == '_'))
        {
            sb.Append(expression[i]);
            i++;
        }

        // There may be space between function name and arguments
        while (i < length && char.IsWhiteSpace(expression[i])) i++;

        position = i - 1;

        if (i < length && expression[i] == '(')
        {
            type = TokenType.Function;
        }

        return new Token(type, sb.ToString(), start);
    }
}
