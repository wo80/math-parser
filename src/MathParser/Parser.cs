﻿namespace MathParser;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public class Parser
{
    #region Static options

    /// <summary>
    /// Gets or sets the <see cref="NumberFormatInfo"/> used to parse numbers.
    /// </summary>
    public static NumberFormatInfo NumberFormat { get; set; } = CultureInfo.InvariantCulture.NumberFormat;

    /// <summary>
    /// Gets or sets the function argument separator (default = ',').
    /// </summary>
    public static char ArgumentSeparator { get; set; } = ',';

    /// <summary>
    /// Gets or sets a value indicating whether to auto-discover variables (default = false).
    /// </summary>
    public static bool AutoVariables { get; set; } = false;

    /// <summary>
    /// Gets the constants dictionary.
    /// </summary>
    public static Dictionary<string, double> Constants { get; } = new Dictionary<string, double>()
    {
        { "pi", Math.PI },
        { "e",  Math.E }
    };

    /// <summary>
    /// Gets the <see cref="FunctionRegistry"/>.
    /// </summary>
    public static FunctionRegistry Functions { get; } = new FunctionRegistry();

    #endregion

    private readonly Dictionary<string, double> variables;

    private Token[] expression;

    /// <summary>
    /// Initializes a new instance of the <see cref="Parser"/> class.
    /// </summary>
    /// <param name="expression">The input expression.</param>
    /// <param name="variables">The variable names.</param>
    private Parser(string expression, IEnumerable<string> variables)
    {
        this.variables = new Dictionary<string, double>();

        Initialize(expression, variables);
    }

    public static Expression Parse(string expression)
    {
        return Parse(expression, Enumerable.Empty<string>());
    }

    public static Expression Parse(string expression, IEnumerable<string> variables)
    {
        var parser = new Parser(expression, variables);

        return new Expression(parser.expression, parser.variables);
    }

    private void Initialize(string expression, IEnumerable<string> variables)
    {
        if (string.IsNullOrEmpty(expression))
        {
            throw new ArgumentException("Input expression cannot be empty", nameof(expression));
        }

        foreach (var name in variables)
        {
            if (Constants.ContainsKey(name))
            {
                throw new InvalidOperationException("Variable name already registered as a constant: " + name);
            }

            this.variables.Add(name, 0.0);
        }

        var token = Scanner.Tokenize(expression);

        ToPostfix(token);
    }

    // The Shunting-Yard algorithm:

    /// <summary>
    /// Converts the input token array in infix notation to postfix notation.
    /// </summary>
    private void ToPostfix(Token[] input)
    {
        int length = input.Length;

        if (input[length - 1].Type == TokenType.Operator)
        {
            // We don't support postfix operators, so the last token can never be one.
            throw new ParserException("Invalid postfix operator", input[length - 1]);
        }

        // The output stack.
        var output = new List<Token>(length);

        // Temporary operator stack.
        var operators = new Stack<Token>(length);

        // For all tokens in the array
        for (int i = 0; i < length; i++)
        {
            var token = input[i];

            if (token.Type == TokenType.Operand)
            {
                // Encountered two successive operands.
                if (i > 0 && input[i - 1].Type == TokenType.Operand)
                {
                    throw new ParserException("Invalid operand token", token);
                }

                // Enqueue operand
                output.Add(token);
            }
            else if (token.Type == TokenType.Symbol)
            {
                if (Constants.TryGetValue(token.Text, out double value))
                {
                    output.Add(new Token(TokenType.Operand, token.Text, token.Position, value));
                }
                else if (variables.ContainsKey(token.Text))
                {
                    output.Add(token);
                }
                else if (AutoVariables)
                {
                    variables.Add(token.Text, 0d);

                    output.Add(token);
                }
                else
                {
                    throw new ParserException("Undefined symbol", token);
                }
            }
            else if (token.Type == TokenType.Function)
            {
                if (Functions.IsKnown(token.Text))
                {
                    operators.Push(token);
                }
                else
                {
                    throw new ParserException("Undefined function", token);
                }
            }
            else if (token.Type == TokenType.Operator)
            {
                if (Operator.IsUnaryMinus(token.Text))
                {
                    operators.Push(token);
                    continue;
                }

                if (operators.Count > 0)
                {
                    int a = Operator.GetPrecedence(token.Text);
                    int b = Operator.GetPrecedence(operators.Peek().Text);

                    bool leftAssociative = Operator.IsLeftAssociative(token.Text);

                    while (operators.Count > 0 && (a < b || (a == b && leftAssociative)))
                    {
                        // Pop operator from stack and enqueue
                        output.Add(operators.Pop());

                        if (operators.Count > 0)
                        {
                            b = Operator.GetPrecedence(operators.Peek().Text);
                        }
                    }
                }

                // Push operator to stack
                operators.Push(token);
            }
            else if (token.Type == TokenType.Separator)
            {
                // Pop operators from stack until there is an opening parenthesis
                while (operators.Count > 0 && !operators.Peek().Text.Equals("("))
                {
                    output.Add(operators.Pop());
                }
            }
            else if (token.Text.Equals("("))
            {
                // Push to stack
                operators.Push(token);
            }
            else if (token.Text.Equals(")"))
            {
                // Pop operators from stack until there is an opening parenthesis
                while (operators.Count > 0)
                {
                    var t = operators.Peek();

                    if (t.Type == TokenType.Parenthesis && t.Text.Equals("("))
                    {
                        operators.Pop();
                        break;
                    }

                    output.Add(operators.Pop());
                }

                // Check if a function is on top the stack
                if (operators.Count > 0 && operators.Peek().Type == TokenType.Function)
                {
                    output.Add(operators.Pop());
                }
            }
        }

        // Pop the remaining operators from stack and enqueue them
        while (operators.Count > 0)
        {
            if (operators.Peek().Type == TokenType.Parenthesis)
            {
                throw new ParserException("Invalid parenthesis", operators.Peek());
            }

            output.Add(operators.Pop());
        }
        
        expression = output.ToArray();
    }
}