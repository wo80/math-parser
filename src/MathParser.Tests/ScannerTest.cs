namespace MathParser.Tests;

using MathParser;
using NUnit.Framework;

[DefaultFloatingPointTolerance(1e-12)]
public class ScannerTest
{
    [TestCase("1", 1.0)]
    [TestCase("2.0", 2.0)]
    [TestCase(".50", 0.50)]
    [TestCase("2.0e2", 2.0e2)]
    [TestCase("2.0e-2", 2.0e-2)]
    [TestCase("2.0e+2", 2.0e+2)]
    [TestCase("2.0e-02", 2.0e-02)]
    [TestCase("2e05", 2e05)]
    [TestCase("2e-05", 2e-05)]
    public void TestNumbers(string input, double expected)
    {
        var tokens = Scanner.Tokenize(input);

        Assert.That(tokens.Length, Is.EqualTo(1));
        Assert.That(tokens[0].Value, Is.EqualTo(expected));
    }

    [TestCase("1.0", new int[] { 0 })]
    [TestCase("-2.0", new int[] { 1, 0 })]
    [TestCase("1+1", new int[] { 0, 1, 0 })]
    [TestCase("1--1", new int[] { 0, 1, 1, 0 })]
    [TestCase("-2+4", new int[] { 1, 0, 1, 0 })]
    [TestCase("3^2", new int[] { 0, 1, 0 })]
    [TestCase("3^-2", new int[] { 0, 1, 1, 0 })]
    [TestCase("3^2^3", new int[] { 0, 1, 0, 1, 0 })]
    [TestCase("(3^2)^3", new int[] { 2, 0, 1, 0, 2, 1, 0 })]
    [TestCase("cos(1)", new int[] { 4, 2, 0, 2 })]
    [TestCase("min(2, 3)", new int[] { 4, 2, 0, 5, 0, 2 })]
    [TestCase("sin(pi/2)", new int[] { 4, 2, 3, 1, 0, 2 })]
    [TestCase("pi*sqrt(4*x^2)", new int[] { 3, 1, 4, 2, 0, 1, 3, 1, 0, 2})]
    [TestCase("-log(sin(pi/2)^2)^2", new int[] { 1, 4, 2, 4, 2, 3, 1, 0, 2, 1, 0, 2, 1, 0 })]
    public void TestExpressions(string input, int[] tokenTypes)
    {
        var tokens = Scanner.Tokenize(input);

        int length = tokenTypes.Length;

        Assert.That(tokens.Length, Is.EqualTo(length));

        for (int i = 0; i < length; i++)
        {
            Assert.That((int)tokens[i].Type, Is.EqualTo(tokenTypes[i]));
        }
    }

    //[TestCase("(( ))", "Invalid parenthesis", 3)]
    [TestCase("1+1#", "Unsupported input character", 3)]
    [TestCase("(1+1", "Unbalanced parenthesis", 4)]
    [TestCase("1+1)", "Unbalanced parenthesis", 3)]
    [TestCase("1e  ", "Invalid floating point format", 2)]
    [TestCase("1e+ ", "Invalid floating point format", 3)]
    [TestCase("1e-e", "Invalid floating point format", 3)]
    [TestCase(".e-1", "Invalid floating point format", 1)]
    public void TestInvalidExpressions(string input, string message, int position)
    {
        var e = Assert.Throws<ScannerException>(() => Scanner.Tokenize(input));

        Assert.That(e.Message, Is.EqualTo(message));
        Assert.That(e.Position, Is.EqualTo(position));
    }

    [TestCase(" 1.1.1 ", new int[] { 0, 0 })]
    [TestCase(" 1,1 ", new int[] { 0, 5, 0 })]
    [TestCase("-(+)-", new int[] { 1, 2, 1, 2, 1 })]
    [TestCase("1   1", new int[] { 0, 0 })]
    [TestCase("1+*+1", new int[] { 0, 1, 1, 1, 0 })]
    [TestCase("sin 0", new int[] { 3, 0 })]
    public void TestInvalidExpressionsLegalSyntax(string input, int[] tokenTypes)
    {
        var tokens = Scanner.Tokenize(input);

        int length = tokenTypes.Length;

        Assert.That(tokens.Length, Is.EqualTo(length));

        for (int i = 0; i < length; i++)
        {
            Assert.That((int)tokens[i].Type, Is.EqualTo(tokenTypes[i]));
        }
    }
}
