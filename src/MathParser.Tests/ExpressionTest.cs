﻿namespace MathParser.Tests;

using MathParser;
using NUnit.Framework;
using System;

[DefaultFloatingPointTolerance(1e-12)]
public class ExpressionTest
{

    [TestCase("sin(pi/2*x) * -cos(0)", "[pi 2 /] x * sin [[0 cos] ~] *")]
    [TestCase("max(1, -cos(min(0, pi/2)))", "[[[[[pi 2 /] 0 min] cos] ~] 1 max]")]
    public void TestSimplify(string input, string expected)
    {
        var e = Parser.Parse(input, new[] { "x" });

        Assert.That(e.Simplify().ToString(), Is.EqualTo(expected));
    }

    [TestCase("sin(pi/2*x) * -cos(0)")]
    public void TestCompile(string input)
    {
        var e = Parser.Parse(input, new[] { "x" });

        var func = e.Simplify().Compile1();

        Assert.That(func(1.0), Is.EqualTo(-1.0));
    }

    [TestCase("sin(pi/2 * x * y)")]
    public void TestCompileN(string input)
    {
        var e = Parser.Parse(input, new[] { "x", "y" });

        var func = e.Simplify().Compile<Func<double, double, double>>();

        Assert.That(func(2.0, 0.5), Is.EqualTo(1.0));
    }

    [Test]
    public void TestCompile1UserFunc0()
    {
        Parser.Functions.Add("one", () => 1);

        var e = Parser.Parse("x + one()", new[] { "x" });

        var func = e.Simplify().Compile1();

        Assert.That(func(1.0), Is.EqualTo(2.0));
    }

    [Test]
    public void TestCompile1UserFunc1()
    {
        Parser.Functions.Add("add", (a) => a + 1);

        var e = Parser.Parse("1 + add(x)", new[] { "x" });

        var func = e.Simplify().Compile1();

        Assert.That(func(1.0), Is.EqualTo(3.0));
    }

    [Test]
    public void TestCompile1UserFunc2()
    {
        Parser.Functions.Add("add", (a, b) => a + b);

        var e = Parser.Parse("1 + add(1, x)", new[] { "x" });

        var func = e.Simplify().Compile1();

        Assert.That(func(1.0), Is.EqualTo(3.0));
    }

    [Test]
    public void TestCompile2UserFunc2()
    {
        Parser.Functions.Add("add", (a, b) => a + b);

        var e = Parser.Parse("1 + add(x, y)", new[] { "x", "y" });

        var func = e.Simplify().Compile2();

        Assert.That(func(0.0, 1.0), Is.EqualTo(2.0));
    }
}
