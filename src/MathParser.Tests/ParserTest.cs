namespace MathParser.Tests;

using MathParser;
using NUnit.Framework;
using System;
using System.Collections.Generic;

[DefaultFloatingPointTolerance(1e-12)]
public class ParserTest
{
    [TestCase("1+1", 2)]
    [TestCase("-2+4", 2)]
    [TestCase("2-1", 1)]
    [TestCase("2*2", 4)]
    [TestCase("4/2", 2)]
    [TestCase("3^2", 9)]
    [TestCase("3^-2", 0.11111111111111111)]
    [TestCase("3^2^3", 6561)]
    [TestCase("(3^2)^3", 729)]
    [TestCase("1+2-3", 0)]
    [TestCase("1-2+3", 2)]
    [TestCase("4-2-2", 0)]
    [TestCase("3*2-5", 1)]
    [TestCase("-5+2*2", -1)]
    [TestCase("10/5*2", 4)]
    [TestCase("5*2/10", 1)]
    [TestCase("10/5+2", 4)]
    [TestCase("2+10/5", 4)]
    [TestCase("3*-2", -6)]
    [TestCase("--2---2", 0)]
    [TestCase("-(2+1)+4", 1)]
    [TestCase("9/-(2+1)", -3)]
    [TestCase("2*(3-1)", 4)]
    [TestCase("(3-1)*2", 4)]
    [TestCase("4/(3-1)", 2)]
    [TestCase("(3+1)/2", 2)]
    [TestCase("(1+1)*(3-1)", 4)]
    [TestCase("(3+1)/(3-1)", 2)]
    public void TestIntegerExpressions(string input, double value)
    {
        var e = Parser.Parse(input);

        Assert.That(e.Evaluate(), Is.EqualTo(value));
    }

    [TestCase("1.5+.25", 1.5 + 0.25)]
    [TestCase(".25+1.5", 0.25 + 1.5)]
    [TestCase("-.25+0.75", -0.25 + 0.75)]
    [TestCase("0.75-.25", 0.75 - 0.25)]
    [TestCase(".75-0.25", 0.75 - 0.25)]
    [TestCase("0.5*1.5", 0.5 * 1.5)]
    [TestCase(".1*.2", 0.1 * 0.2)]
    [TestCase("1.5/0.5", 1.5 / 0.5)]
    [TestCase(".05/.5", 0.05 / 0.5)]
    [TestCase("1.2^2", 1.44)]
    [TestCase("9^.5", 3.0)]
    [TestCase("1.2*2.5/.2", 1.2 * 2.5 / 0.2)]
    [TestCase("1.2/2.5/3.2", 1.2 / 2.5 / 3.2)]
    [TestCase("1.2*-2.5+-3.2", 1.2 * -2.5 + -3.2)]
    [TestCase("1.2*.5^2", 1.2 * 0.25)]
    [TestCase("1.2*.5^2/0.5", 1.2 * 0.25 / 0.5)]
    [TestCase("(2.4*.5)/1.25", (2.4 * 0.5) / 1.25)]
    [TestCase("(3.2+.8)^-2", 0.0625)]
    [TestCase("(3.2+.8)^-2*4", 0.0625 * 4)]
    [TestCase("1.2*.5^2/(1/3)", 1.2 * 0.25 / (1.0 / 3.0))]
    public void TestFloatExpressions(string input, double value)
    {
        var e = Parser.Parse(input);

        Assert.That(e.Evaluate(), Is.EqualTo(value));
    }

    [Test]
    public void TestFunctions()
    {
        var functions = new Dictionary<string, double>
        {
            { "cos(1)", Math.Cos(1.0) },
            { "sin(1)", Math.Sin(1.0) },
            { "tan(1)", Math.Tan(1.0) },
            { "acos(1)", Math.Acos(1.0) },
            { "asin(1)", Math.Asin(1.0) },
            { "atan(1)", Math.Atan(1.0) },
            { "atan2(1, 2)", Math.Atan2(1.0, 2.0) },
            { "cosh(1)", Math.Cosh(1.0) },
            { "sinh(1)", Math.Sinh(1.0) },
            { "tanh(1)", Math.Tanh(1.0) },
            { "acosh(1)", Math.Acosh(1.0) },
            { "asinh(1)", Math.Asinh(1.0) },
            { "atanh(1)", Math.Atanh(1.0) },
            { "hypot(2, 3)", Double.Hypot(2.0, 3.0) },
            { "exp(1)", Math.Exp(1.0) },
            { "log(6)", Math.Log(6.0) },
            { "log2(6)", Math.Log2(6.0) },
            { "log10(6)", Math.Log10(6.0) },
            { "sqrt(2)", Math.Sqrt(2.0) },
            { "cbrt(2)", Math.Cbrt(2.0) },
            { "abs(-1)", Math.Abs(-1.0) },
            { "pow(2, 3)", Math.Pow(2.0, 3.0) },
            { "min(2, 3)", Math.Min(2.0, 3.0) },
            { "max(2, 3)", Math.Max(2.0, 3.0) }
        };

        foreach (var item in functions)
        {
            var input = item.Key;
            var value = item.Value;

            var e = Parser.Parse(input);

            Assert.That(e.Evaluate(), Is.EqualTo(value), input);
        }
    }

    [Test]
    public void TestFunctions2()
    {
        var functions = new Dictionary<string, double>
        {
            { "sin(pi/2)", Math.Sin(Math.PI/2.0) },
            { "cos(pi/2)", Math.Cos(Math.PI/2.0) },
            { "tan(pi/4)", Math.Tan(Math.PI/4.0) },
            { "2*exp(1)-e", 2.0*Math.Exp(1)-Math.E },
            { "log(e)", Math.Log(Math.E) },
            { "sin(-pi/2)", Math.Sin(-Math.PI/2.0) },
            { "-sin(pi/2)", -Math.Sin(Math.PI/2.0) },
            { "-sin(-pi/2)", -Math.Sin(-Math.PI/2.0) },
            { "sin(cos(1))", Math.Sin(Math.Cos(1.0)) },
            { "cos(-sin(1))", Math.Cos(-Math.Sin(1.0)) },
            { "-cos(sin(-1))", -Math.Cos(Math.Sin(-1.0)) }
        };

        foreach (var item in functions)
        {
            var input = item.Key;
            var value = item.Value;

            var e = Parser.Parse(input);

            Assert.That(e.Evaluate(), Is.EqualTo(value), input);
        }
    }

    [TestCase("1+1", 2.0)]
    [TestCase("2*3-2", 4.0)]
    [TestCase("2*(3-2)", 2.0)]
    [TestCase("(-(2*2)^(3-2))", -4.0)]
    [TestCase("4^2^3", 65536.0)]
    [TestCase("4^(2^3)", 65536.0)]
    [TestCase("(4^2)^3", 4096.0)]
    [TestCase("mod(5,3)", 2.0)]
    [TestCase("pi", Math.PI)]
    [TestCase("-log(sin(pi/2)^2)^2", 0.0)]
    [TestCase("max(1, 2)", 2.0)]
    public void TestExpressions(string input, double value)
    {
        var e = Parser.Parse(input);

        Assert.That(e.Evaluate(), Is.EqualTo(value));
    }

    [TestCase("sin(0.5*pi*x)^2", new[] { 1.0, 2.0 }, new[] { 1.0, 0.0 })]
    [TestCase("max(0.0, min(sin(.5*pi*x), cos(.5*pi*x)))", new[] { 0.0, 1.0, 2.0 }, new[] { 0.0, 0.0, 0.0 })]
    public void TestExpressionsVar1(string input, double[] x, double[] expected)
    {
        var e = Parser.Parse(input, new[] { "x" });

        int length = x.Length;

        Assert.That(length, Is.EqualTo(expected.Length));

        for (int i = 0; i < length; i++)
        {
            e.Variables["x"] = x[i];

            Assert.That(e.Evaluate(), Is.EqualTo(expected[i]));
        }
    }

    [TestCase("cos(0.5*pi*x*y)", new[] { 1.0, 2.0 }, new[] { 1.0, -1.0 }, new[] { 0.0, -1.0 })]
    public void TestExpressionsVar2(string input, double[] x, double[] y, double[] expected)
    {
        var e = Parser.Parse(input, new[] { "x", "y" });

        int length = x.Length;

        Assert.That(length, Is.EqualTo(expected.Length));

        for (int i = 0; i < length; i++)
        {
            e.Variables["x"] = x[i];
            e.Variables["y"] = y[i];

            Assert.That(e.Evaluate(), Is.EqualTo(expected[i]));
        }
    }

    [Test]
    public void TestAutoDiscoverVars()
    {
        var exception = Assert.Throws<ParserException>(() => Parser.Parse("1+x"));

        Assert.That(exception.Message, Is.EqualTo("Undefined symbol"));
        Assert.That(exception.Token.Text, Is.EqualTo("x"));

        Parser.AutoVariables = true;

        var e = Parser.Parse("1+x");

        Assert.That(e.Variables.ContainsKey("x"));
    }

    [Test]
    public void TestDuplicateSymbol()
    {
        Parser.Constants.Add("one", 1.0);
        Parser.Functions.Add("one", () => 1.0);

        var e = Parser.Parse("one + one()");

        Assert.That(e.Evaluate(), Is.EqualTo(2.0));

        var exception = Assert.Throws<InvalidOperationException>(() => Parser.Parse("one + one()", new[] { "one" }));

        Assert.That(exception.Message, Is.EqualTo("Variable name already registered as a constant: one"));
    }

    [Test]
    public void TestUserDefinedFunction()
    {
        var exception = Assert.Throws<ParserException>(() => Parser.Parse("time()"));

        Assert.That(exception.Message, Is.EqualTo("Undefined function"));
        Assert.That(exception.Token.Text, Is.EqualTo("time"));

        Parser.Functions.Add("time", () => DateTimeOffset.Now.ToUnixTimeSeconds());

        var e = Parser.Parse("time()");

        var t0 = e.Evaluate();
        var t1 = DateTimeOffset.Now.ToUnixTimeSeconds();

        Assert.That(t1 - t0, Is.LessThanOrEqualTo(1.0));
    }

    [Test]
    public void TestTooManyFunctionArgs()
    {
        var e = Parser.Parse("max(1,2,3)");
        var exception = Assert.Throws<ParserException>(() => e.Evaluate());

        Assert.That(exception.Message, Is.EqualTo("Error evaluating postfix expression"));
        Assert.That(exception.Token, Is.Null);
    }
}
