# Math Parser for .NET

A simple math expression parser written in C#. For details see

* [Math Expression Parser (1/2)](http://wo80.bplaced.net/posts/2020/03-math-parser-1/)
* [Math Expression Parser (2/2) - Optimization](http://wo80.bplaced.net/posts/2020/03-math-parser-2/)

A typescript version is available [here](https://gitlab.com/wo80/math-parser-ts).

## Examples

Evaluate math expression:

```csharp
var e = Parser.Parse("sin(pi/2)");
var value = e.Evaluate();
```

Using variables:

```csharp
var e = Parser.Parse("sin(pi/2*x)", new[] { "x" });
e.Variables["x"] = 1.0;
var value = e.Evaluate();
```

Compile expression to IL:

```csharp
var e = Parser.Parse("x + y", new[] { "x", "y" });
var f = e.Compile<Func<double, double, double>>();
var value = f(1.0, 1.0);
```

Take a look at the [unit tests](src/MathParser.Tests) for more examples.
